<?php

namespace yii2portal\rss;

use Yii;

class Module extends \yii2portal\core\Module
{
    public $miniLogo = '';
    public $squareLogo = '';
    public $assetClassName = '';

    public $controllerNamespace = 'yii2portal\rss\controllers';

}