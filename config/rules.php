<?php


return [
    [
        'pattern' => "mailru_partner",
        'route' => "index/mailru-partner"
    ],
    [
        'pattern' => "mailru",
        'route' => "index/mailru"
    ],
    [
        'pattern' => "yandex",
        'route' => "index/yandex"
    ],
    [
        'pattern' => "sitemap",
        'route' => "index/index"
    ],
    [
        'pattern' => "all",
        'route' => "index/index"
    ],
    [
        'pattern' => "",
        'route' => "index/index"
    ],
];