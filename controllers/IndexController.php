<?php

namespace yii2portal\rss\controllers;

use Yii;
use yii\filters\ContentNegotiator;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Response;
use yii2portal\core\controllers\Controller;
use yii2portal\news\models\News;

/**
 * Site controller
 */
class IndexController extends Controller
{

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\PageCache',
                'only' => ['yandex', 'mailru-partner', 'mailru', 'index'],
                'dependency' => [
                    'class' => 'yii2portal\news\components\CacheDependency',
                ],
                'duration' => 60 * 60 * 5,
            ],
            [
                'class' => ContentNegotiator::className(),
                'only' => ['mailru-partner'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ]
        ];
    }


    public function actionIndex($structure_id)
    {
        Yii::$app->response->format = Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'text/xml');

        $news = News::find()
            ->with('imageLenta', 'newsAgency', 'newsAgency.imageLogo')

            ->andPublished()
            ->orderBy(['datepublic' => SORT_DESC])
            ->byDatepublic()
            ->limit(50)
            ->all();


        return $this->renderPartial('index', [
            'page' => Yii::$app->getModule('structure')->getPage($structure_id),
            'news' => $news
        ]);
    }


    public function actionYandex($structure_id)
    {
        Yii::$app->response->format = Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'text/xml');

        $pages = array_filter(Yii::$app->getModule('structure')->getPagesByModule('news'), function($page){
            return $page->getParamValue('is_yandex', 'off') == 'on';
        });

        $news = News::find()
            ->where([
                'pid'=>ArrayHelper::getColumn($pages,'id')
            ])
            ->with('imageLenta')
            ->andWhere([
                'in_yandex_rss' => 1
            ])
            ->andPublished()
            ->orderBy(['datepublic' => SORT_DESC])
            ->byDatepublic(mktime(0, 0, 0, date('n'), date('j') - 8, date('Y')), time())
            ->all();


        return $this->renderPartial('yandex', [
            'page' => Yii::$app->getModule('structure')->getPage($structure_id),
            'news' => $news
        ]);
    }

    public function actionMailru($structure_id)
    {
        Yii::$app->response->format = Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'text/xml');

        $pages = array_filter(Yii::$app->getModule('structure')->getPagesByModule('news'), function($page){
            return $page->getParamValue('is_yandex', 'off') == 'on';
        });

        $news = News::find()
            ->where([
                'pid'=>ArrayHelper::getColumn($pages,'id')
            ])
            ->with('imageLenta')
            ->andPublished()
            ->orderBy(['datepublic' => SORT_DESC])
            ->byDatepublic(mktime(0, 0, 0, date('n'), date('j') - 4, date('Y')), time())
            ->all();


        return $this->renderPartial('mailru', [
            'page' => Yii::$app->getModule('structure')->getPage($structure_id),
            'news' => $news
        ]);
    }


    public function actionMailruPartner($structure_id)
    {
        $pages = array_filter(Yii::$app->getModule('structure')->getPagesByModule('news'), function($page){
            return $page->getParamValue('is_yandex', 'off') == 'on';
        });


        $imagedNew = News::find()
            ->where([
                'pid'=>ArrayHelper::getColumn($pages,'id')
            ])
            ->withAnyImage()
            ->andPublished()
            ->byDatepublic()
            ->andWhere(['>', 'image', 0])
            ->orWhere(['>', 'image_col', 0])
            ->orWhere(['>', 'image_lenta', 0])
            ->orWhere(['>', 'image_main', 0])
            ->orderBy(['datepublic' => SORT_DESC])
            ->one();

        $otherNews = News::find()
            ->where([
                'pid'=>ArrayHelper::getColumn($pages,'id')
            ])
            ->withAnyImage()
            ->andPublished()
            ->byDatepublic()
            ->andWhere(['not', ['id' => $imagedNew->id]])
            ->orderBy(['datepublic' => SORT_DESC])
            ->limit(2)
            ->all();

        $news = array_merge(array($imagedNew), $otherNews);
        $data = [];

        foreach ($news as $new) {
            $joJson = array(
                'title' => $new->mailru_title?$new->mailru_title:$new->title,
                'datetime' => $new->dateTimeFormat('d MMMM'),
                'url' => Url::to($new->urlPath, true),
            );
            $image = $new->anyImage;
            if ($image) {
                $joJson['img'] = Url::to($image->thumbnail(70, 50), true);
            }
            $data['news'][] = $joJson;
        }

        return $data;
    }


}
