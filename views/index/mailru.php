<?php

use \yii\helpers\Url;
use \yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $page \yii2portal\structure\models\CoreStructure */
/* @var $news \yii2portal\news\models\News[] */

$assetClassName = Yii::$app->getModule('rss')->assetClassName;

$bundle = $assetClassName::register($this);

?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>

<rss version="2.0" xmlns:mailru="http://news.mail.ru/">

    <channel>

        <title><?php echo Yii::$app->name ?></title>
        <link><?php echo Url::to('/', true); ?></link>
        <language>ru</language>
        <lastBuildDate><?php echo date("r")?></lastBuildDate>

        <?php foreach ($news as $new): ?>
            <item>
                <guid><?php echo Url::to($new->urlPath, true); ?></guid>
                <category><?php echo $new->parent->title; ?></category>
                <title><![CDATA[<?php echo $new->title; ?>]]></title>
                <description><![CDATA[<?php echo $new->newsDescription; ?>]]></description>
                <mailru:full-text><![CDATA[<?php echo $new->content; ?>]]></mailru:full-text>
                <pubDate><?php echo date('r', $new->datepublic); ?></pubDate>
                <link><?php echo Url::to($new->urlPath, true); ?></link>
                <?php if ($new->imageLenta): ?>
                    <enclosure url='<?php echo Url::to($new->imageLenta->srcUrl, true) ?>' type='<?php echo $new->imageLenta->content_type?>' length='<?php echo $new->imageLenta->file_size;?>'/>
                <?php endif; ?>
            </item>
        <?php endforeach; ?>
    </channel>
</rss>
