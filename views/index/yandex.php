<?php

use \yii\helpers\Url;
use \yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $page \yii2portal\structure\models\CoreStructure */
/* @var $news \yii2portal\news\models\News[] */

$assetClassName = Yii::$app->getModule('rss')->assetClassName;

$bundle = $assetClassName::register($this);

?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>

<rss xmlns:yandex="http://news.yandex.ru" xmlns:media="http://search.yahoo.com/mrss/" version="2.0">

    <channel>

        <title><?php echo Yii::$app->name ?></title>

        <link><?php echo Url::to('/', true); ?></link>

        <image>

            <url><?php echo Url::to($bundle->baseUrl . Yii::$app->getModule('rss')->miniLogo, true); ?></url>

            <title><?php echo Yii::$app->name ?></title>

            <link><?php echo Url::to('/', true); ?></link>

        </image>

        <yandex:logo type="square"><?php echo Url::to($bundle->baseUrl . Yii::$app->getModule('rss')->squareLogo, true); ?></yandex:logo>

        <?php foreach ($news as $new): ?>
            <item>

                <title><?php echo Html::encode($new->title); ?></title>
                <link><?php echo Url::to($new->urlPath, true); ?></link>
                <description><?php echo Html::encode($new->newsDescription); ?></description>
                <category><?php echo $new->parent->title; ?></category>
                <?php if ($new->imageLenta): ?>
                    <enclosure url='<?php echo Url::to($new->imageLenta->srcUrl, true) ?>' type='<?php echo $new->imageLenta->content_type?>' length='<?php echo $new->imageLenta->file_size;?>'/>
                <?php endif; ?>
                <pubDate><?php echo date('r', $new->datepublic); ?></pubDate>

                <yandex:full-text><?php echo Html::encode($new->content); ?></yandex:full-text>

            </item>
        <?php endforeach; ?>
    </channel>
</rss>
